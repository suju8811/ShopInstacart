
import requests,json, queue, threading, time
import faster_than_requests as frequests
from hmac_headers import HMACHeaders
from login import AccessToken
import uuid, datetime, os, gzip, http.client

def log_message(log):
    print(log)


class Main(HMACHeaders, AccessToken):

    def __init__(self):
        HMACHeaders.__init__(self)
        AccessToken.__init__(self)

        # self.response_status = []
        # self.offerIds = set()
        # self.auto_stop = False


    # def check_blocks(self):
    #
    #     running_start_time = time.time_ns()
    #     while 1:
    #         refresh_headers = {}
    #         hmac_header = self.new_post_getoffer(self.access_token)
    #         refresh_headers.update(hmac_header)
    #         refresh_headers.update(self.headers)
    #         refresh_headers.update(self.base_headers)
    #
    #         tuple_header = list(refresh_headers.items())
    #
    #         frequests.setHeaders(tuple_header)
    #         start_time = time.time_ns()
    #         check_block_resp = frequests.gets(self.refresh_block_url)
    #         time_consumed = time.time_ns() - start_time
    #
    #         response_code = int(check_block_resp["status"][0:3])
    #
    #         if response_code == 200:
    #             self.response_status.append(True)
    #
    #         else:
    #             log_message("%-20s : %d" % ("  Status code", response_code))
    #             if response_code == 400:
    #                 self.response_status.append(False)
    #
    #
    #         if response_code == 403:
    #             EMAIL = self.settings.get('EMAIL')
    #             PASSWORD = self.settings.get('PASSWORD')
    #             SOURCE_TOKEN = self.settings.get('SOURCE_TOKEN')
    #             access_token = self.update_token(EMAIL, PASSWORD, SOURCE_TOKEN)
    #             log_message(access_token)
    #             if access_token:
    #                 self.access_token = access_token
    #             self.response_status = []
    #             continue
    #
    #         if response_code == 400:
    #             time.sleep(1)
    #             continue
    #
    #         raw_body = check_block_resp["body"]
    #         resp_body = {}
    #
    #         try:
    #             resp_body = json.loads(gzip.decompress(raw_body))
    #         except:
    #             resp_body = json.loads(raw_body)
    #
    #         offers_list = resp_body["offerList"] #check_block_resp.json()['offerList']
    #         final_offers = []
    #
    #         if final_offers:
    #             print('seen')
    #
    #         offers_list = [offer for offer in offers_list if offer['offerId'] not in self.offerIds]
    #
    #         if self.auto_stop:
    #             break
    #
    #         elif self.settings.get('SLEEP_TIME') > time_consumed/1000000000:
    #            time.sleep(self.settings.get('SLEEP_TIME')-time_consumed/1000000000)
    #
    #         log_message("%-20s : %fs" % ("  Time", time_consumed/1000000000))
    #
    #         if len(offers_list) > 0:
    #             with open('offers.txt', 'a+', encoding='utf-8') as (save_offers):
    #                 for offer in offers_list:
    #                     save_offers.write(str(offer))
    #                     save_offers.write("\n")
    #                     self.offerIds.add(offer['offerId'])
    #
    #
    # def cb_wrapper(self):
    #     while True:
    #         if self.auto_stop:
    #             break
    #         try:
    #             self.check_blocks()
    #         except Exception as e:
    #             print(e)
    #
    #             with open('errors.txt', 'a+') as (t):
    #                 t.write(str(e))
    #                 t.write("\n")


    def start_bot(self):
            
        EMAIL = self.settings.get('EMAIL')
        PASSWORD = self.settings.get('PASSWORD')
        SOURCE_TOKEN = self.settings.get('SOURCE_TOKEN')
        access_token = self.update_token(EMAIL, PASSWORD, SOURCE_TOKEN)

        self.flex_instance_id = self.settings.get('FLEX_INSTANCE_ID')
        if access_token:
            self.access_token = access_token
            log_message("%-20s : %s" % ("AccessToken", access_token))
            for i in range(int(self.settings.get('THREADS'))):
                th = threading.Thread(target=self.cb_wrapper)
                th.start()


alpha = Main()
alpha.start_bot()
