@extends('layouts.main')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>{{ ucfirst($pageName) }}</h2>
    </header>
    <!--start content  -->
    <div style="margin: -40px">
        @if(Auth::user()->subscription_expired)              
            <div class="row" style="padding: 40px" >
                <div class="col-xs-12 col-lg-12 alert alert-danger" style="text-align: center;">
                    <span class="text-primary;" style="text-align: center;color: red">It seems your subscription is expired. Go to Make a Payment to renew it.</span> 
                </div>
            </div>
        @else
        <div class="row" >
            <div class="col-xs-12 col-lg-12">
                <h2 style="text-align: center;color: #1e9c9d">Virtual Batches</h2>
                <div class="row" style="margin-top:30px">
                    <div class="col-xs-6 col-lg-6"><a class="btn" name="running_status" style="background-color: @if(Auth::user()->is_running) #179e4d @else gray @endif;color: white;float: right;text-transform: uppercase">running</a></div>                    
                    <div class="col-xs-6 col-lg-6"><a class="btn" name="stopped_status" style="background-color: @if(Auth::user()->is_running) gray @else red @endif;color: white;float: left;text-transform: uppercase">stopped</a></div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
        <div class="row" style="margin: 20px">
            <div class="card-body">
                <form class="form-horizontal form-bordered" style="padding: 20px" method="get" action="/">                    
                    <div class="form-group row" >
                        <div class="col-lg-offset-4 col-lg-4" style="padding: 0 15px;">
                            <section class="form-group-vertical">
                                <div class="input-group input-group-icon">
                                    <span class="input-group-addon">
                                        <span class="icon"><i class="fa fa-phone"></i></span>
                                    </span>
                                    <input type="text" class="form-control input-rounded" placeholder="Phone Number:" value="{{ 'Phone Number: +1'.Auth::user()->phonenumber }}" disabled>
                                </div>
                                <div class="input-group input-group-icon">
                                    <span class="input-group-addon">
                                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                                    </span>
                                    <input type="text" class="form-control input-rounded" placeholder="Current location:"  value="{{ 'Current location: '.$info['latitude'].', '.$info['longitude'] }}" disabled>
                                </div>
                                <div class="input-group input-group-icon">
                                    <span class="input-group-addon">
                                        <span class="icon"><i class="fa fa-shopping-cart"></i></span>
                                    </span>
                                    <input type="text" class="form-control input-rounded" placeholder="Batch type:" value="{{ 'Batch type: '.$info['deliveryService'] }}"  disabled>
                                </div>                                
                            </section>
                        </div>                       
                    </div>
                    <footer class="card-footer" style="margin-top: 30px">
                        <div class="row justify-content-end">
                            <div class="col-xs-12" style="text-align:center">
                                <a class="btn btn-default turn-on" style="width: 100px" @if(Auth::user()->is_running) disabled @endif;>Turn On</a>
                                <a class="btn btn-default turn-off" style="width: 100px" >Turn Off</a>
                                <a href="{{ url('/home') }}" class="btn btn-default" style="width: 100px">Edit</a>
                            </div>
                        </div>
                    </footer> 
                    <div class="col-lg-offset-2 col-lg-8" style="margin-top:50px">
                        <!-- http://52.0.126.208 -->
                        <h5 style="text-align: center;color: black;">
                            <div id ="div_batches_accepted_count">        
                                Batches Accepted: 
                            </div>                            
                        </h5>                                             
                        <iframe src="http://{{Auth::user()->ip_address}}/getAcceptedBatches.php?phone={{Auth::user()->phonenumber}}" frameborder="1"
                            style="border:1;width: 100%" scrolling="auto"></iframe>                            
                    </div>
                </form>
            </div>
        </div>
        <input type="hidden" name="phonenumber" value="{{Auth::user()->phonenumber }}">
        <input type="hidden" name="password" value="{{$info['password'] }}">
        <input type="hidden" name="ip_address" value="{{Auth::user()->ip_address }}">
        <input type="hidden" name="extension" value="{{Auth::user()->extension }}">        
        <input type="hidden" name="latitude" value="{{$info['latitude'] }}">
        <input type="hidden" name="longitude" value="{{$info['longitude'] }}">
        <input type="hidden" name="editEstimate" value="{{$info['editEstimate'] }}">
        <input type="hidden" name="deliveryService" value="{{$info['deliveryService'] }}">
        <input type="hidden" name="running" value="{{Auth::user()->is_running }}">                
        @endif
    </div>
    
<!--end content  -->
</section>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        function load_home (e) {
            (e || window.event).preventDefault();
        
            fetch("http://" + $("[name='ip_address']").val() + "/getAcceptedBatches.php?phone=" + $("[name='phonenumber']").val() + "&type=count" /*, options */)
            .then((response) => response.text())
            // .then((html) => {
            //     document.getElementById("div_batches_accepted_count").innerHTML = "Today Accepted: "+html;
            // })
            .then(function(data) {
                document.getElementById("div_batches_accepted_count").innerHTML = "Today Accepted: "+data;
            })
            .catch((error) => {
                console.warn(error);
            });
        } 
        load_home();
    });
</script>
@endsection
