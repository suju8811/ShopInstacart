import hashlib
import json
import time
import uuid
from decimal import Decimal
from re import sub
import jwt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from jwt.utils import base64url_encode, base64url_decode
from send_sms import *
import requests
import urllib3
import datetime
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


settings = json.load(open("settings.json"))
s = settings['PHONE'] + settings['PASSWORD']
m = hashlib.sha1(s.encode("UTF-8"))
nonce = base64url_encode(m.digest()) + b"="
nonce = nonce.decode()
print(nonce)

header = {"x5c": ["MIIFkzCCBHugAwIBAgIRANcSkjds5n6+CAAAAAApa0cwDQYJKoZIhvcNAQELBQAwQjELMAkGA1UEBhMCVVMxHjAcBgNVBAoTFUdvb2dsZSBUcnVzdCBTZXJ2aWNlczETMBEGA1UEAxMKR1RTIENBIDFPMTAeFw0yMDAxMTMxMTQxNDlaFw0yMTAxMTExMTQxNDlaMGwxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRMwEQYDVQQKEwpHb29nbGUgTExDMRswGQYDVQQDExJhdHRlc3QuYW5kcm9pZC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCWErBQTGZGN1iZbN9ehRgifWBxqi2Pdgxw03P7TyJZfMxjp5L7j1GNePK5HzdrUoId1yCIyBMyxqgazqgtpX5WpsXW4VfMhJbN1Y09qzqp6JD+2PZdoTU1kFRAMWfL/UuZtk7pmRXgGm5jKDrZ9Nxe04vMYQr88NqwW/kfZ1gTONIUT0WsLT/4522BRWxfwxc3QE1+TKWkLCrvek6WlIqyaC52W7MDR8MpFebymSKTvwfMRwyKQLT03UL4vt48yEc8sp7wTAHM/WDg8Qotarf8OBHknoZ92XiviaV6tQqhROHCfgmnCXixfW0wEXCvqiLTbQtUbLsS/8IRtdXkpQB9AgMBAAGjggJYMIICVDAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUHAwEwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQU6DHBwsAvb53g/C07prTvvwNQQLYwHwYDVR0jBBgwFoAUmNH4bhDrz5vsYJ8YkBug630J/SswZAYIKwYBBQUHAQEEWDBWMCcGCCsGAQUFBzABhhtodHRwOi8vb2NzcC5wa2kuZ29vZy9ndHMxbzEwKwYIKwYBBQUHMAKGH2h0dHA6Ly9wa2kuZ29vZy9nc3IyL0dUUzFPMS5jcnQwHQYDVR0RBBYwFIISYXR0ZXN0LmFuZHJvaWQuY29tMCEGA1UdIAQaMBgwCAYGZ4EMAQICMAwGCisGAQQB1nkCBQMwLwYDVR0fBCgwJjAkoCKgIIYeaHR0cDovL2NybC5wa2kuZ29vZy9HVFMxTzEuY3JsMIIBBAYKKwYBBAHWeQIEAgSB9QSB8gDwAHcA9lyUL9F3MCIUVBgIMJRWjuNNExkzv98MLyALzE7xZOMAAAFvnuy0ZwAABAMASDBGAiEA7e/0YRu3wAFmWH27M2vbVcZ/mrp+4rfYc/5IPJ29F6gCIQCnKCCAacVNeYZ8CCfYdGpB2GsHxuMOHka/O41jWeF+zgB1AESUZS6w7s6vxEAH2Kj+KMDa5oK+2MsxtT/TM5a1toGoAAABb57stJMAAAQDAEYwRAIgEXbioPbJp9qC0Dj258DFGSRMAU+ZB1EiVEbbb/4UvNECIBhHkBt18vRn9zDvyrfxyudcHTOSl3gTaYA/7yT/BiH4MA0GCSqGSIb3DQEBCwUAA4IBAQDIAcQBlmd8MEgLdrrrMbBTCvpMXst5+wx2DlfajJNJUP4jYFjYUQ9B3X4E2zf49nX3AyuZFxAqORnbj/5jkY7a8qMJ0j19zFOB+qerxec0nhm8gYlLbQm6sKY7P0exfr7HuK3MkP1pec14wFEUaGqDwUbGgl/oiz38FXCE+CW8E1QAEUfvbQPTYbKxYj+tCNlss0bTSoL2Z2d/j3BpL3MFw0yxSK/UTqykLr2A/MdhJQmxi+G+MKRSsQr62AnZau9q6YFoi+9AEH+A48XtIyshLyCTU3Ht+aKohGnxA5ul1XRmqp8HvcAt39P95FZGFJe0uvlyjOwAzXuMu7M+PWRc","MIIESjCCAzKgAwIBAgINAeO0mqGNiqmBJWlQuDANBgkqhkiG9w0BAQsFADBMMSAwHgYDVQQLExdHbG9iYWxTaWduIFJvb3QgQ0EgLSBSMjETMBEGA1UEChMKR2xvYmFsU2lnbjETMBEGA1UEAxMKR2xvYmFsU2lnbjAeFw0xNzA2MTUwMDAwNDJaFw0yMTEyMTUwMDAwNDJaMEIxCzAJBgNVBAYTAlVTMR4wHAYDVQQKExVHb29nbGUgVHJ1c3QgU2VydmljZXMxEzARBgNVBAMTCkdUUyBDQSAxTzEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDQGM9F1IvN05zkQO9+tN1pIRvJzzyOTHW5DzEZhD2ePCnvUA0Qk28FgICfKqC9EksC4T2fWBYk/jCfC3R3VZMdS/dN4ZKCEPZRrAzDsiKUDzRrmBBJ5wudgzndIMYcLe/RGGFl5yODIKgjEv/SJH/UL+dEaltN11BmsK+eQmMF++AcxGNhr59qM/9il71I2dN8FGfcddwuaej4bXhp0LcQBbjxMcI7JP0aM3T4I+DsaxmKFsbjzaTNC9uzpFlgOIg7rR25xoynUxv8vNmkq7zdPGHXkxWY7oG9j+JkRyBABk7XrJfoucBZEqFJJSPk7XA0LKW0Y3z5oz2D0c1tJKwHAgMBAAGjggEzMIIBLzAOBgNVHQ8BAf8EBAMCAYYwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMBIGA1UdEwEB/wQIMAYBAf8CAQAwHQYDVR0OBBYEFJjR+G4Q68+b7GCfGJAboOt9Cf0rMB8GA1UdIwQYMBaAFJviB1dnHB7AagbeWbSaLd/cGYYuMDUGCCsGAQUFBwEBBCkwJzAlBggrBgEFBQcwAYYZaHR0cDovL29jc3AucGtpLmdvb2cvZ3NyMjAyBgNVHR8EKzApMCegJaAjhiFodHRwOi8vY3JsLnBraS5nb29nL2dzcjIvZ3NyMi5jcmwwPwYDVR0gBDgwNjA0BgZngQwBAgIwKjAoBggrBgEFBQcCARYcaHR0cHM6Ly9wa2kuZ29vZy9yZXBvc2l0b3J5LzANBgkqhkiG9w0BAQsFAAOCAQEAGoA+Nnn78y6pRjd9XlQWNa7HTgiZ/r3RNGkmUmYHPQq6Scti9PEajvwRT2iWTHQr02fesqOqBY2ETUwgZQ+lltoNFvhsO9tvBCOIazpswWC9aJ9xju4tWDQH8NVU6YZZ/XteDSGU9YzJqPjY8q3MDxrzmqepBCf5o8mw/wJ4a2G6xzUr6Fb6T8McDO22PLRL6u3M4Tzs3A2M1j6bykJYi8wWIRdAvKLWZu/axBVbzYmqmwkm5zLSDW5nIAJbELCQCZwMH56t2Dvqofxs6BBcCFIZUSpxu6x6td0V7SvJCCosirSmIatj/9dSSVDQibet8q/7UK4v4ZUN80atnZz1yg=="]}
data = {'nonce': nonce, 'timestampMs': int(time.time())*1000, 'apkPackageName': 'com.instacart.shopper', 'apkDigestSha256': 'glzVZeYIqnzLzMKY97L7XwmUKPYS+KbvGYL3xOF5k2w=', 'ctsProfileMatch': True, 'apkCertificateDigestSha256': ['R5YYFBqaJDkhUR4QDYzS44fTNg0apF2wk/jOw4DwQAE='], 'basicIntegrity': True, 'advice': 'RESTORE_TO_FACTORY_ROM'}
private_key = rsa.generate_private_key(
    backend=default_backend(),
    public_exponent=65537,
    key_size=2048
)
signature = jwt.encode(data, private_key, algorithm='RS256', headers=header)
signature = signature.decode()
print(signature)


fp = open("batches.json")
batches_str = fp.read()
batches_str = batches_str.replace("\n", "")
if len(batches_str) > 3:
    batches_str = batches_str[0:len(batches_str) - 1]
batches_str = "[" + batches_str + "]"
cur_batches = json.loads(batches_str)

Token_url = 'https://shopper-api.instacart.com/oauth/token.json'
RegisteringDevice_url = 'https://shopper-api.instacart.com/shopper_devices.json'
Needs_location_url = 'https://locations-api.instacart.com/locations/needs_location.json'
Location_url = 'https://locations-api.instacart.com/locations.json'
Batches_url = 'https://shopper-api.instacart.com/driver/virtual_batches'


header_batches = json.load(open("header_batches.json"))
header = json.load(open("header.json"))
proxies = {}
if settings['USE_PRX']:
    proxies = {
        'http': 'http://' + settings['USER_PRX'] + ':' + settings['PASSWORD_PRX'] + '@' + settings['IP_PRX'] + ':' +
                settings['PORT_PRX'],
        'https': 'http://' + settings['USER_PRX'] + ':' + settings['PASSWORD_PRX'] + '@' + settings['IP_PRX'] + ':' +
                 settings['PORT_PRX']
    }
    print(requests.get("http://lumtest.com/myip.json", proxies=proxies).text)


def login():
    expires = 0
    access_token = 'Basic QmFzaWMgTW1Nek5ETXdOR1F4WldFNFlUaGxOVFkyTWpabE1HRmpPRGt6T1dWaU5EZzJZekE0WVRsbE5EQmtNR0V4TVROaE9UVm1OelpsWXpFNU5qQTVOMkUwTnpwak0yVXhaREkzWVRVMk9ESmhOakUwWTJVNE5qQm1Zak0yT0ROallqbGhObU00WkRkbVpUQmxOVEU1Wm1NeFpHSmxNall5TW1GbFkyVXlZMkUyTmpVNA=='
    # 1. Auth login to get Token
    data_for_token = [
        ('client_id', settings['CLIENT_ID']),
        ('client_secret', settings['CLIENT_SECRET']),
        ('grant_type', 'password'),
        ('kochava_device_id', settings['KOCHAVA_DEVICE_ID']),
        ('password', settings['PASSWORD']),
        ('scope', 'driver'),
        ('username', settings['PHONE']),
        ('signature', signature)
    ]
    print("1. Post " + Token_url)
    # header['Accept'] = 'application/json'
    header['X-Request-ID'] = str(uuid.uuid4())
    response = requests.post(Token_url, headers=header, data=data_for_token, proxies=proxies)
    # response = requests.post(Token_url, headers=header, data=data_for_token)
    if response.status_code == 200:
        print('Token Success!')
        print(response.text)
        response_token_json = json.loads(response.text)
        if 'access_token' in response_token_json:
            access_token = "Bearer " + response_token_json['access_token']
        if 'expires_in' in response_token_json:
            expires = response_token_json['expires_in']
    else:
        print("%d: %s" % (response.status_code, response.reason))

    print(access_token)
    header_batches['Authorization'] = access_token
    header_batches['X-Request-ID'] = str(uuid.uuid4())
    # 2. Registering Device
    data_for_register_device = [
        ('device_token', settings['DEVICE_TOKEN']),
        ('device_type', 'android')
    ]
    print("2. Post " + RegisteringDevice_url)
    response = requests.post(RegisteringDevice_url, headers=header_batches, data=data_for_register_device,
                             proxies=proxies)
    if response.status_code == 200:
        print('Register Device Success!')
        print(response.text)
    else:
        print("%d: %s" % (response.status_code, response.reason))

    header_batches['If-None-Match'] = 'W/"89379b3a31c4bf6089f457389eda8b06"'
    response = requests.get(Needs_location_url, headers=header_batches, proxies=proxies)
    if response.status_code == 200:
        print('Needs location Success!')
        print(response.text)
    else:
        print("%d: %s" % (response.status_code, response.reason))

    # 3. Getting Location info

    now_date = datetime.datetime.now()
    sixteen_hours_from_now = now_date + datetime.timedelta(hours=16)
    data_for_location = [
        ('accuracy', 3.9),
        ('direction', 0.0),
        ('is_considered_mock', False),
        ('is_mock', False),
        ('latitude', settings['LOCATION_LATITUDE']),
        ('longitude', settings['LOCATION_LONGITUDE']),
        ('measured_at', str(now_date)),
        ('speed', 0.0),
        ('track_location_response', True),
        ('true_measured_at', str(sixteen_hours_from_now))
    ]

    print("3 Post " + Location_url)
    response = requests.post(Location_url, headers=header_batches, data=data_for_location, proxies=proxies)
    if response.status_code == 200:
        print('Location Success!')
        print(response.text)
        # response_json = json.loads(response.text)
    else:
        print("%d: %s" % (response.status_code, response.reason))
    return expires


# {
#   "date":"15-11-2019|09:32 AM",
#   "store":"Publix",
#   "location":"Publix #223 - Publix Super Market",
#   "price":"$28.01",
#   "status":"Not Accepted"
# },
# old_batch_price = 0
# old_batch_store = ''
# old_batch_uuid = ''


def save_batch(batch, status):
    # global old_batch_price
    # global old_batch_store
    # global old_batch_uuid
    global cur_batches
    # if old_batch_price == batch['earnings']['estimate'] and old_batch_store == batch['warehouse']['name']:
    for x in cur_batches:
        if cur_batches[x]['id'] == batch['id']:
            return
    now1 = datetime.datetime.now()
    my_date = datetime.datetime.strptime(str(now1.hour) + ":" + str(now1.minute), "%H:%M")
    date = now1.strftime('%d-%m-%y') + "|" + my_date.strftime("%I:%M %p")
    price = batch['earnings']['estimate']
    store = batch['warehouse']['name']
    batch_id = batch['id']
    # location = batch['suggested_warehouse_location']['name']
    msg = "{\"date\":\"" + date + "\",\"id\":\"" + batch_id + "\",\"store\":\"" + store + "\",\"price\":\"" + price + "\",\"status\":\"" + status + "\"}"
    batches_file = open('batches.json', 'a')
    batches_file.write(msg + ",\n")
    batches_file.close()
    cur_batches.append(json.loads(msg))


def accept_batch(batch_uuid, batch_accept):
    print("5 Post " + Batches_url + "/" + batch_uuid + "/accept?batch_id_only=true")
    response_accept = requests.post(Batches_url + "/" + batch_uuid + "/accept?batch_id_only=true",
                                    headers=header_batches)
    if response_accept.status_code == 200:
        print('Batches Accept Success!')
        print(response_accept.text)

        # now1 = datetime.datetime.now()
        # my_date = datetime.datetime.strptime(str(now1.hour) + ":" + str(now1.minute), "%H:%M")
        # msg = now1.strftime('%d/%m/%y') + " - " + my_date.strftime("%I:%M %p") + " - Batch paid for " + \
        #       batch_accept['earnings']['estimate'] + ",at " + batch_accept['warehouse']['name'] + " located in " + \
        #       batch_accept['suggested_warehouse_location']['name'] + "\n"
        # batches_accept_file = open('batches_accepted.txt', 'a')
        # batches_accept_file.write(msg)
        # batches_accept_file.close()
        save_batch(batch_accept, "Accepted")
        try:
            send_sms(settings['PHONE'], batch_accept)
        except:
            print("send sms exception")
        return True
    else:
        print("%d: %s" % (response_accept.status_code, response_accept.reason))
        save_batch(batch_accept, "Not Accepted")
        return False


expires_in = login()
expires_in = expires_in - 2000

# i = 0
# flag = True
now = datetime.datetime.now()
is_auto_off = False
# print
# for x in header_batches:
#    print("%s: %s" % (x, header_batches[x]))
while True:
    try:
        settings = json.load(open("settings.json"))
        if settings['REQUEST_STOP'] or is_auto_off:
            break
        # print("4 Get " + Batches_url + " " + str(datetime.datetime.now()))
        print("4 Get " + Batches_url)
        header_batches['If-None-Match'] = 'W/"1d8f80f5473d4e400a8aaba4978839ca"'
        response_batches = requests.get(Batches_url, headers=header_batches, proxies=proxies)
        print(response_batches.elapsed.total_seconds())

        if response_batches.status_code == 200:
            try:
                response_json = json.loads(response_batches.text)
                virtual_batches = response_json['data']['virtual_batches']
                if len(virtual_batches) > 0:
                    print('Batches Success!')
                    print(response_batches.text)
                    # batches_file = open('batches.txt', 'a')
                    # batches_file.write(str(datetime.datetime.now()) + " " + response_batches.text + "\n")
                    # batches_file.close()
                    for batch in virtual_batches:
                        batch_type = batch['batch_type']
                        calledAcceptBatch = False
                        if (settings['DELIVERY_SERVICE'] == 'Delivery Only & Full Service') or (
                                (settings['DELIVERY_SERVICE'] == 'Delivery Only') and (
                                batch_type == 'delivery_only')) or \
                                ((settings['DELIVERY_SERVICE'] == 'Full Service') and (batch_type == 'delivery')):
                            price = Decimal(sub(r'[^\d.]', '', batch['earnings']['estimate']))
                            if price > settings['MINIMUM_PRICE']:
                                if 'uuid' in batch:
                                    calledAcceptBatch = True;
                                    if accept_batch(batch['uuid'], batch) and settings['AUTO_OFF']:
                                        settings['REQUEST_STOP'] = True
                                        with open('settings.json', 'w') as f:
                                            json.dump(settings, f)
                                        # is_auto_off = True
                                        url = 'http://www.easyhours.net/runOff?phone=' + settings['PHONE']
                                        requests.get(url)
                                        break
                                    # print("")
                        if not calledAcceptBatch:
                            save_batch(batch, "Not Accepted")
            except:
                print("no json")
        else:
            print("%d: %s" % (response_batches.status_code, response_batches.reason))
            # if someone login by apk, close script
            if response_batches.status_code == 401:
                if settings['AUTO_LOGIN']:
                    expires_in = login()
                    expires_in = expires_in - 2000
                    now = datetime.datetime.now()
                    continue
                else:
                    break

        later = datetime.datetime.now()
        difference_seconds = (later - now).total_seconds()
        # if token is expired, then relogin
        if (expires_in != 0) and (expires_in < difference_seconds * 1000):
            expires_in = login()
            expires_in = expires_in - 2000
            now = datetime.datetime.now()
            continue
        time.sleep(settings['REQUEST_PAUSE_TIME'])
        if difference_seconds > 3000:
            expires_in = login()
            expires_in = expires_in - 2000
            now = datetime.datetime.now()
    except:
        expires_in = login()
        expires_in = expires_in - 2000
        now = datetime.datetime.now()
