@extends('layouts.main')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>{{ ucfirst($pageName) }}</h2>
    </header>
    <!--start content  -->
    <div style="margin: -40px">
        @if(Auth::user()->subscription_expired)              
            <div class="row " style="padding: 40px;" >
                <div class="col-xs-12 col-lg-12 alert alert-danger" style="text-align: center;">
                    <span class="text-primary;" style="text-align: center;color: red">It seems your subscription is expired. Go to Make a Payment to renew it.</span> 
                </div>
            </div>
        @else        
        <div class="row" >
            <div class="col-xs-12 col-lg-12">
                <h2 style="text-align: center;color: #1e9c9d">Preferences</h2>
            </div>
        </div>
        <div class="row" style="margin: 20px">
            <div class="card-body">
                <form class="form-horizontal form-bordered" style="padding: 20px" onsubmit="return validateForm()" method="post" action="home/postContinue">
                    @csrf
                    <div class="row" style="text-align: center;margin-bottom:15px;">
                        <h5 class="text-lg-center" style="text-align: center;" >Current location:</h5>
                        <div style="display: inline-block;margin-right:10px;width:220px;">
                            <select id="location_type" class="form-control input-sm" style="border-radius:15px;height:35px"> 
                                <option value="" disabled selected>Please select location method</option>
                                <option value="location_manual">Select your location manually</option>
                                <option value="location_auto">Get your location automatically</option>                                               
                            </select>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <div style="display: inline-block;margin-right:10px;width:220px">
                            <section class="form-group-vertical" >
                                <div class="input-group input-group-icon" >
                                    <span class="input-group-addon" >
                                        <span class="icon" style="font-size:13px;padding-top:9px" >LAT:</span>
                                    </span>                                    
                                    <input value="{{ $info['latitude']}}" name="latitude" type="text" class="form-control input-sm" style="padding-left:46px;border-radius:15px;height:35px" id="inputLatitude"  placeholder="Latitude" required>
                                </div>                               
                            </section>                            
                        </div>
                        <div style="display: inline-block;margin-right:10px;width:220px">
                            <section class="form-group-vertical">
                                <div class="input-group input-group-icon" >
                                    <span class="input-group-addon" >
                                        <span class="icon" style="font-size:13px;padding-top:9px">LON:</span>
                                    </span>                                    
                                    <input value="{{ $info['longitude']}}" name="longitude" type="text" class="form-control input-sm" style="padding-left:46px;border-radius:15px;height:35px" id="inputLongitude"  placeholder="Longitude" required>
                                </div>                               
                            </section>                            
                        </div>
                    </div>
                    <div id="div_btn_panel" class="form-group row">
                        <div class="col-lg-12 text-center" style="margin-top:10px">
                            <div id="btn_cur_location" style="display:none;">
                                <a  class="btn btn-primary current_location" style="border-radius:15px;">Current Location</a>
                            </div>
                            <div id="btn_center_map" style="display:none;">
                                <a id="btn_center_map" class="btn btn-primary center_map" style="border-radius:15px;">Center Map</a>
                            </div>    
                        </div>                            
                    </div>

                    <div id="div_map" class="form-group row" style="display:none;">
                        <div id="map-container" class="col-lg-12" style="height: 200px;border:2px solid #d6d6d6;margin-left: 15px;margin-right: 15px">
                        </div>
                    </div>

                    <div class="form-group row" style="text-align:center">
                        <h5 class="text-lg-center" style="padding-left: 0px;border-radius:15px;" >Minimum batch estimate:</h5>
                        <div style="display: inline-block;width:220px">
                            <section class="form-group-vertical">
                                <div class="input-group input-group-icon" >
                                    <span class="input-group-addon">
                                        <span class="icon" style="padding-top:7px"><i class="fa fa-dollar"></i></span>
                                    </span>
                                    <input value="{{$info['editEstimate']}}" id="editEstimate" name="editEstimate" class="form-control" style="border-radius:15px;height:35px" type="text" placeholder="estimate" value="12" min="0" max="35" required>
                                    <!-- <span style="margin-top:10px">&nbsp;&nbsp;[Here we will limit until 35]</span> -->
                                </div>                               
                            </section>
                        </div>
                    </div>
                    <div class="form-group row" style="text-align:center">
                        <h5 class="text-lg-center" style="padding-left: 0px" >Service type:</h5>
                        <div style="display: inline-block;width:220px">
                            <select id="service_type" name="service_type" class="form-control input-sm" style="border-radius:15px;height:35px"> 
                                <!-- <option value="" disabled selected>Select service</option> -->
                                <option value="" disabled selected>Select Service</option>
                                @if( $info['deliveryService'] === 'Full Service' )
                                    <option id="full_service" value="Full Service" selected>Full Service</option>
                                @else
                                    <option id="full_service" value="Full Service">Full Service</option>
                                @endif
                                @if( $info['deliveryService'] === 'Delivery Only' )
                                    <option id="delivery_only" value="Delivery Only" selected>Delivery Only</option>
                                @else
                                    <option id="delivery_only" value="Delivery Only">Delivery Only</option>
                                @endif     
                                @if( $info['deliveryService'] === 'Delivery Only & Full Service' )
                                    <option id="both_service" value="Delivery Only & Full Service" selected>Delivery Only & Full Service</option>
                                @else
                                    <option id="both_service" value="Delivery Only & Full Service">Delivery Only & Full Service</option>
                                @endif                              
                            </select>
                        </div>
                    </div>
                    <!-- <footer class="card-footer"> -->
                        <div class="row justify-content-end">
                            <div class="col-xs-12" style="text-align:center">
                                <button id="btn_continue" class="btn btn-primary" style="border-radius:15px;">Save and Continue</button>
                            </div>
                        </div>
                    <!-- </footer>  -->
                </form>
            </div>
            <input type="hidden" name="phonenumber" value="{{Auth::user()->phonenumber }}">
            <input type="hidden" name="password" value="{{$info['password'] }}">
            <input type="hidden" name="ip_address" value="{{Auth::user()->ip_address }}">
        </div>
        @endif
    </div>
<!--end content  -->
</section>
@endsection
@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUKBHq7QEAIjV8xpERcEnnvi9y1iE0Bpc"></script>
<script type="text/javascript" src="{{ asset('assets/js/home.js') }}"></script>
<script>
    function validateForm() {
        if(document.getElementById('inputLatitude').value == 0 || document.getElementById('inputLongitude').value == 0)
        {
            alert("Please set your location");
            return false;
        }
        else if(document.getElementById('service_type').value == "") {
            alert("Please select service");
            return false;
        }

        $.ajax({
			url: $domain + "/userScriptTurnOnOff.php",
            type:'get',
            data:{
				type:0,
            	phonenumber:$("[name='phonenumber']").val(),
            	password:$("[name='password']").val(),
				latitude:document.getElementById('inputLatitude').value,
				longitude:document.getElementById('inputLongitude').value,
				editEstimate:$("[name='editEstimate']").val(),
				deliveryService:document.getElementById('service_type').value
            },
            success: function(result){
            	if(result=="success"){
                    					
            	} else {
            	}
            },
			error: function(jqXHR, exception){
				is_clicked = false;
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
			  	alert('Turn On Failed\n' + msg);
			}
        });
    }
</script>    
@endsection
