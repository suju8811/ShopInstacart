var pageName;
var message_list_display = "";
$(function(){
	
	var $domain = "http://localhost";
	$domain = "http://" + $("[name='ip_address']").val();
    var assets_path = $("#assets_path").val();
    var base_url = $("#base_url").val();
    pageName = $("#pageName").val();
    var user_name = $("[name='name']").val();
	var user_phone = $("[name='phone']").val();
	var is_clicked = false;
    $(".change-password-btn").click(function(){
    	$(this).css("display","none");
    	$(".change-password").css("display","block");
    });
    $(".change-profile").click(function(){

    	$(".change-password-btn").css("display","inline");
    	$(".change-password").css("display","none");
    	var st1=0;
    	var st2=0;
    	var npassword, cpassword;
    	$(".change-password").each(function(){
    		if($(this).find('input').attr('name')=='npassword'){
    			npassword = $(this).find('input').val();
    		}
    		if($(this).find('input').attr('name')=='cpassword'){
    			cpassword = $(this).find('input').val();
    		}
    		if($(this).find('input').attr('name')=='opassword'){
    			opassword = $(this).find('input').val();
    		}
    		if(!$(this).find('input').val().length){
    			st1=1;
    		} else {
    			st2=1;
    		}
    		$(this).find('input').val("");
    	});
    	if(st1==1 && st2==1){
    		message(msg.ErrorChangePassword,"warning");
    		return false;
    	}
    	// alert($("[name='name']").val()+","+$("[name='cpassword']").val())
    	if(npassword!=cpassword){
    		message(msg.MisMatchPassword,"warning");
    		return false;
    	}

	    $.ajax({
            url: "saveProfile",
            type:'get',
            data:{
            	name:$("[name='name']").val(),
				phone:$("[name='phone']").val(),
				email:$("[name='email']").val(),
            	npassword:npassword,
            	opassword:opassword,
            },
            success: function(result){
            	if(result=="success"){
            		message(msg.SuccessSaveProfile,"success");
            		user_name = $("[name='name']").val();
				    user_phone = $("[name='phone']").val();
				    $(".profile-info .name").text(user_phone);
            	} else {
            		message(msg.FailedSaveProfile,"warning");
            	}
            }
        });
    });
    $(".modal-dismiss").click(function(){
    	$(".change-password-btn").css("display","inline");
    	$(".change-password").css("display","none");
    	$(".change-password").each(function(){
    		$(this).find('input').val("");
    	});
    	$("[name='name']").val(user_name);
    	$("[name='phone']").val(user_phone);
	});
	
	$(".turn-on").click(function(){
		// http://52.0.126.208/+17868179221/7868179221.exe	
		// alert('Turn On');
		if(is_clicked == true) return;
		is_clicked = true;
		if($("[name='running']").val() == '1') {
			return;
		}
		$.ajax({
			url: $domain + "/userScriptTurnOnOff.php",
            type:'get',
            data:{
				type:0,
            	phonenumber:$("[name='phonenumber']").val(),
            	password:$("[name='password']").val(),
				latitude:$("[name='latitude']").val(),
				longitude:$("[name='longitude']").val(),
				editEstimate:$("[name='editEstimate']").val(),
				deliveryService:$("[name='deliveryService']").val()
            },
            success: function(result){
            	if(result=="success"){
					$.ajax({
						url: $domain + "/+1"+$("[name='phonenumber']").val()+"/run.php",
						type:'get',
						data:{
							phonenumber:$("[name='phonenumber']").val(),
							extension:$("[name='extension']").val()
						},
						success: function(result){

						},
						error: function(jqXHR, exception){
							is_clicked = false;
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							alert('Turn On Failed\n' + msg);
						}
					});
					// message(msg.SuccessTurnOn,"success");
					$.ajax({
						url: "turnOn",
						type:'get',
						success: function(result){
							location.reload();
							is_clicked = false;
						}
					});
            	} else {
					is_clicked = false;
					alert(result);
					message(msg.FailedTurnOn,"warning");
            	}
            },
			error: function(jqXHR, exception){
				is_clicked = false;
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
			  	alert('Turn On Failed\n' + msg);
			}
        });
	});

	$(".turn-off").click(function(){
		// alert("Turn Off");
		if($("[name='running']").val() == '0') {
			return;
		}
		$.ajax({
            url: $domain + "/userScriptTurnOnOff.php",
            type:'get',
            data:{
				type:1,
            	phonenumber:$("[name='phonenumber']").val()
            },
            success: function(result){
            	if(result=="success"){
					// message(msg.SuccessTurnOff,"success");
					$.ajax({
						url: "turnOff",
						type:'get',
						success: function(result){
							
							location.reload();							
						}
					});
            	} else {
            		message(msg.FailedTurnOff,"warning");
            	}
            },
			error: function(jqXHR, exception){
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
			  	alert('Turn Off Failed\n' + msg);
			}
        });
	});
});

