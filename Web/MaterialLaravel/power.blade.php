@extends('layouts.main')
@section('content')
<div class="content">        
    <div class="container-fluid">        
    <!--start content  -->
    
        @if(Auth::user()->subscription_expired)              
            <div style="margin: -40px">
                <div class="row" style="padding: 40px" >
                    <div class="col-xs-12 col-lg-12 alert alert-danger" style="text-align: center;">
                        <span class="text-primary;" style="text-align: center;color: white">It seems your subscription is expired. Go to Make a Payment to renew it.</span> 
                    </div>
                </div>
            </div>
        @else
        @if($info['model']['active'] == 0)
        <div class="goto_configuration" style="margin: -40px;display:block">
            <div class="row" style="padding: 40px" >
                <div class="col-xs-12 col-lg-12 alert alert-danger" style="text-align: center;">
                    <span class="text-primary;" style="text-align: center;color: white">{{$info['model']['message']}}
                    </span>                     
                </div>
            </div>
        </div>
        @else
        <div class="goto_configuration" style="margin: -40px;display:none">
            <div class="row" style="padding: 40px" >
                <div class="col-xs-12 col-lg-12 alert alert-danger" style="text-align: center;">
                    <span class="text-primary;" style="text-align: center;color: white">You have to configure your preferences before using the system,
                    <!-- <a type="submit" class="btn btn-primary btn-link btn-lg" style="margin:0px;padding:0px;">{{ __('Redirect to Configuration') }}</a> -->
                    <a href="{{ url('home') }}" class="text-light">
                            <small>{{ __('Redirect to Configuration') }}</small>
                        </a>
                    </span> 
                    
                </div>
            </div>
        </div>
        <div class="main_screen" style="display:none">
        <div class="row justify-content-center" >
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">info_outline</i>                            
                            </div>
                    </div>                    
                    <div class="row justify-content-center" style="margin-top:0.8rem;margin-bottom:0.8rem;">
                        <div class="col-md-12" style="text-align:center">
                            <button class="btn btn-twitter turn-on" style="padding-left:20px;padding-right:20px;"  @if(Auth::user()->is_running) disabled @endif;>Turn On</button>
                            <button class="btn btn-facebook turn-off" style="padding-left:20px;padding-right:20px;">Turn Off</button>
                        </div>
                    </div>
                    <hr style="margin-left:1rem;margin-right:1rem;margin-top:0px;margin-bottom:0px;"/>
                    <div class="card-footer justify-content-center">
                        <div class="stats">
                            
                            @if(Auth::user()->is_running)    
                            <span style="font-size: 0.763rem;">Currently Running&nbsp&nbsp</div>
                            <div style="margin-top:2px">
                                <svg height="12" width="12">
                                    <circle cx="6" cy="6" r="6" stroke="black" stroke-width="0" fill="green" />
                                </svg>
                            </div>
                            @else                            
                            <span style="font-size: 0.763rem;">Currently Stopped&nbsp&nbsp</span>
                            <div style="margin-top:2px">
                                <svg height="12" width="12">
                                    <circle cx="6" cy="6" r="6" stroke="black" stroke-width="0" fill="red" />
                                </svg>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row justify-content-center" >
            <div class="col-md-12" style="text-align:center">
                <button class="btn" name="running_status" style="background-color: @if(Auth::user()->is_running) #179e4d @else gray @endif;color: white;text-transform: uppercase">running</button>
                <button class="btn" name="stopped_status" style="background-color: @if(Auth::user()->is_running) gray @else red @endif;color: white;text-transform: uppercase">stopped</button>
            </div>
        </div> -->

        <!-- <div class="row" >
            <div class="col-xs-12 col-lg-12">
                <h2 style="text-align: center;color: #1e9c9d">Virtual Batches</h2>
                <div class="row" style="margin-top:30px">
                    <div class="col-xs-6 col-lg-6"><a class="btn" name="running_status" style="background-color: @if(Auth::user()->is_running) #179e4d @else gray @endif;color: white;float: right;text-transform: uppercase">running</a></div>                    
                    <div class="col-xs-6 col-lg-6"><a class="btn" name="stopped_status" style="background-color: @if(Auth::user()->is_running) gray @else red @endif;color: white;float: left;text-transform: uppercase">stopped</a></div>
                </div>
                <div style="clear: both"></div>
            </div>
        </div> -->
        <!-- <div class="row" style="margin: 20px"> -->
            <!-- <div class="card-body"> -->
                <!-- <form class="form-horizontal form-bordered" style="padding: 0px" method="get" action="/">                     -->
                    <!-- <footer class="card-footer" style="margin-top: 0px"> -->
                        
                    <!-- </footer>  -->
                        <div class="row justify-content-center">                            
                            <div class="col-md-12">
                                <div class="card" style="font-size: 0.7rem;">
                                    <div class="card-header card-header-primary">
                                        <h5 class="card-title ">Incoming Batches</h5>
                                        <p class="card-category">Below are shown accepted or non-accepted offers</p>
                                    </div>
                                    <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="batches-table" class="table">
                                        <!-- cellspacing="0" width="100%" -->
                                        <thead class=" text-primary">
                                            <th style="font-size: 0.763rem;" width="25%">Date</th>
                                            <th style="font-size: 0.763rem;" width="25%">Store</th>
                                            <th style="font-size: 0.763rem;" width="25%">Price</th>
                                            <th style="font-size: 0.763rem;" width="25%">Status</th>
                                        </thead>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- <div class="col-lg-offset-2 col-lg-8" style="margin-top:50px">
                        <h5 style="text-align: center;color: black;">
                            <div id ="div_batches_accepted_count">        
                                Batches Accepted: 
                            </div>                            
                        </h5>                                             
                        <iframe src="http://{{Auth::user()->ip_address}}/getAcceptedBatches.php?phone={{Auth::user()->phonenumber}}" frameborder="1"
                            style="border:1;width: 100%" scrolling="auto"></iframe>                            
                    </div> -->
                <!-- </form> -->
            <!-- </div> -->
        </div>
        @endif
        @endif
        <input type="hidden" name="phonenumber" value="{{Auth::user()->phonenumber }}">
        <input type="hidden" name="password" value="{{$info['password'] }}">
        <input type="hidden" name="ip_address" value="{{Auth::user()->ip_address }}">
        <input type="hidden" name="extension" value="{{Auth::user()->extension }}">        
        <input type="hidden" name="latitude" value="{{$info['latitude'] }}">
        <input type="hidden" name="longitude" value="{{$info['longitude'] }}">
        <input type="hidden" name="editEstimate" value="{{$info['editEstimate'] }}">
        <input type="hidden" name="deliveryService" value="{{$info['deliveryService'] }}">
        <input type="hidden" name="running" value="{{Auth::user()->is_running }}">  
        <input type="hidden" name="expired" value="{{Auth::user()->subscription_expired }}">
        <input type="hidden" name="active" value="{{$info['model']['active'] }}">
        
        
<!--end content  -->
</div>
</div>
@endsection
@if(Auth::user()->subscription_expired)
@else
@section('script')
<script>
    $(document).ready(function(){
//         function load_home (e) {
//             (e || window.event).preventDefault();
        
// //            fetch("http://" + $("[name='ip_address']").val() + "/getAcceptedBatches.php?phone=" + $("[name='phonenumber']").val() + "&type=count" /*, options */)
//             var url = "http://" + $("[name='ip_address']").val() + "/+1"+$("[name='phonenumber']").val() + "/batches.json";
//             fetch(url)            
//             .then((response) => response.text())
//             // .then((html) => {
//             //     document.getElementById("div_batches_accepted_count").innerHTML = "Today Accepted: "+html;
//             // })
//             .then(function(data) {
//                 //document.getElementById("div_batches_accepted_count").innerHTML = "Today Accepted: "+data;
//             })
//             .catch((error) => {
//                 console.warn(error);
//             });
//         } 
//         load_home();
        
        // $.ajax({
        //     url: "getBatchString",
        //     type:'get',
        //     success: function(result){
                
        //     },
        //     error: function(jqXHR, exception){

        //     }
        // });
        if($("[name='expired']").val() == 1 || $("[name='active']").val() == 0) return;
        $.ajax({
            url: $domain + "/getSettingsJson.php",
            type:'get',
            data:{
                phonenumber:$("[name='phonenumber']").val()
            },
            success: function(result){
                var json = JSON.parse(result);                                                
                // document.getElementByName('password').value = json.PASSWORD;
                // document.getElementByName('latitude').value = json.LOCATION_LATITUDE;
                // document.getElementByName('longitude').value = json.LOCATION_LONGITUDE; 
                // document.getElementByName('editEstimate').value = json.MINIMUM_PRICE;  
                // document.getElementByName('deliveryService').value = json.DELIVERY_SERVICE;                    
                // document.getElementByName('auto_off').value = json.AUTO_OFF;   
                // price 0
                // lat empty
                // lon empty
                // service type empty                    
                if(json.MINIMUM_PRICE==0 || json.DELIVERY_SERVICE=="" || json.LOCATION_LATITUDE=="0" || json.LOCATION_LATITUDE=="" || json.LOCATION_LONGITUDE=="0" || json.LOCATION_LONGITUDE=="")         
                {
                    $(".goto_configuration").css("display","block");
                    $(".main_screen").css("display","none");
                } else {
                    $(".goto_configuration").css("display","none");
                    $(".main_screen").css("display","block");                    
                }
            },
			error: function(jqXHR, exception){
				is_clicked = false;
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
			  	alert('Settings Json Failed\n' + msg);
			}
        });
    });
</script>
<script type="text/javascript" src="{{ asset('assets/js/power.js') }}"></script>
@endsection
@endif