@extends('layouts.main')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/pricing.css') }}">
@endsection
@section('content')
<div class="content">        
    <div class="container-fluid">
<!--start content  -->
    <!-- <div class="row " style="padding: 40px;" >
        <div class="col-md-12 alert alert-success" style="text-align: center;">
            <span class="text-primary;" style="text-align: center;color: white">Momentaneamente, para efectuar tu pago, envialo por Zelle a vpcflex@gmail.com y escribenos al (305) 204-7453 para actualizar tu membresia.</span> 
        </div>
    </div> -->

    <div style="margin: -40px;padding: 20px">
        <form action="charge" method="POST">
            @csrf
            <section class="pricing-table">
                <div class="col-md-5 col-lg-4">
                    <div class="item">
                        <div class="ribbon">Best Value</div>
                        <!-- <div class="heading" >
                            <h3>PRO</h3>
                        </div> -->
                        <p>Make a payment</p>
                        <div class="features">
                            <h4><span class="feature"><span class="value"> </span></h4>
                            <h4><span class="feature">Duration</span> : <span class="value">7 Days</span></h4>
                            <h4><span class="feature"></h4>
                        </div>
                        <div class="price">
                            <h4>${{$stripe_info['amount']/100.0}}</h4>
                        </div>
                        @if(Auth::user()->subscription_expired)                        
                        <!-- <button class="btn btn-block btn-primary" type="submit">BUY NOW</button> -->
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="{{$stripe_info['stripe_pub_key']}}"
                            data-amount="{{$stripe_info['amount']}}"
                            data-name="Instacart"
                            data-description="Proceed to Pay with Card"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-locale="auto"
                            data-currency="{{$stripe_info['currency']}}">
                        </script>
                        <script>
                            // Hide default stripe button, be careful there if you
                            // have more than 1 button of that class
                            document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                        </script>
                        <button class="btn btn-block btn-primary" type="submit">BUY NOW</button>
                        @else
                        @endif
                    </div>
                </div>
            </section>
            <!-- <input type="hidden" name="stripe_pub_key" value="{{env('STRIPE_PUB_KEY')}}"> -->
        </form>    
    </div>
<!--end content  -->
</div>
</div>
@endsection
@section('script')

@endsection
