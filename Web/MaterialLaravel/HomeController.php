<?php 
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsersModel;
use App\Models\StripeOptionModel;
use App\Models\SubscriptionModel;
use App\Models\PrincipleModel;
use App\Models\ContactusModel;
use App\Models\PowerModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth; 
use DateTime;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class HomeController extends Controller
{
	public function index()
	{
        $info['latitude'] = session('latitude', '0.0');
        $info['longitude'] = session('longitude', '0.0');
        $info['editEstimate'] = session('editEstimate', '0.0');
        $info['deliveryService'] = session('deliveryService', "");
        $info['password'] = session('password', "");  
		return view('home',['pageName'=>'Configuration', 'info' => $info]);
    }

    public function postContinue(Request $req)
    {
        if((float)$req->input('latitude') == 0 or (float)$req->input('longitude') == 0 or $req->input('service_type') == "")
        {
            return redirect('home');
        }

        session(['latitude' => (float)$req->input('latitude')]);
        session(['longitude' => (float)$req->input('longitude')]);
        session(['editEstimate' => (float)$req->input('editEstimate')]);
        session(['deliveryService' => $req->input('service_type')]);
        return redirect('power');
    }

    public function showContinue()
    {
        $info['latitude'] = session('latitude', '0.0');
        $info['longitude'] = session('longitude', '0.0');
        $info['editEstimate'] = session('editEstimate', '0.0');
        $info['deliveryService'] = session('deliveryService', "");
        $info['password'] = session('password', "");                   
        $info['model'] = PowerModel::select('*')->orderBy('created_at', 'desc')->get()->first();
        return view('power',['pageName'=>'Power', 'info' => $info]);
    }    
    
    public function saveProfile(Request $request)
    {        
        $name = $request->get('name');
        $city = $request->get('city');
        $phone = $request->get('phone');
        $email = $request->get('email');
    	$opassword = $request->get('opassword');
    	$npassword = $request->get('npassword');
    	if($opassword=="") {
    		UsersModel::where('phonenumber', Auth::user()->phonenumber)->update(['name' => $name, 'email' => $email, 'city' => $city]);
			echo "success";	
    	} else {
    		if(Auth::attempt(['phonenumber' => Auth::user()->phonenumber, 'password' => $opassword])){
				UsersModel::where('phonenumber', Auth::user()->phonenumber)->update(['name' => $name, 'email' => $email, 'city' => $city, 'password' => Hash::make($npassword)]);
				echo "success";	
			} else {
				echo "Old password is not correct";
			}
    	}
    }

    public function turnOn()
    {
        UsersModel::where('phonenumber', Auth::user()->phonenumber)->update(['is_running' => 1]);
    }
    
    public function turnOff()
    {        
        UsersModel::where('phonenumber', Auth::user()->phonenumber)->update(['is_running' => 0]);
    }

    public function runOff(Request $request)
    {
        $phone = $request->get('phone');;
        $phone = str_replace("+1","",$phone);
        if(strlen($phone)>10) {
            $phone = substr($phone,strlen($phone)-10,10);
        }
        UsersModel::where('phonenumber', $phone)->update(['is_running' => 0]);
        //echo $request;
        echo $phone;
    }
    

    public function subscription()
    {        
        $user = UsersModel::where('phonenumber', Auth::user()->phonenumber)->first();
        $curdate = date('Y-m-d H:i:s');
        $datetime1 = new DateTime($curdate);
        $end_date = date($user->subscription_end_date);
        $datetime2 = new DateTime($end_date);

        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%r%a');//now do whatever you like with $days

        $str = strtotime($end_date) - (strtotime($curdate));
        if($str/3600 < 24) $days = 1;
        else $days = $str/3600/24;

        $info['subscription_expired'] = $user->value('subscription_expired');
        $info['days'] = (int)$days;
        
        return $info['days'];
        //return view('subscription',['pageName'=>'Subscription','info' => $info]);
    }

    public function payment()
    {        
        $stripe_info = StripeOptionModel::all()->first();
        return view('payment',['pageName'=>'Make a payment','stripe_info' => $stripe_info]);
    }

    public function charge(Request $request)
    {
        try {
            $stripe_info = StripeOptionModel::all()->first();
            Stripe::setApiKey($stripe_info['stripe_secret_key']);
        
            $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source' => $request->stripeToken
            ));
        
            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => $stripe_info['amount'],
                'currency' => $stripe_info['currency']
            ));
            
            //add subscription
            $curdate = date('Y-m-d H:i:s');
            $endDate=Date('Y-m-d H:i:s', strtotime("+7 days"));
            $user = UsersModel::where('phonenumber', Auth::user()->phonenumber)->first();
            $subscription = new SubscriptionModel;
            $subscription->userid = $user['id'] ;
            $subscription->started_date = $curdate;
            $subscription->amount = $stripe_info['amount'];
            $subscription->currency = $stripe_info['currency'];
            $subscription->save();

            $subarray = SubscriptionModel::where('userid', $user->id)->get();
            $paid_amount = 0;
            foreach ($subarray as $sub)
            {
                $paid_amount += $sub->amount;
            }
            $user->subscription_started_date = $curdate;
            $user->subscription_end_date = $endDate;            
            $user->paid_amount = $paid_amount;
            $user->subscription_expired = 0;            
            $user->save();
            return redirect('/payment');
        } catch (\Exception $ex) {
            //return $ex->getMessage();
            $request->session()->put('message','FailedBuyNow|'.$ex->getMessage());         
            return redirect('/payment');
        }        
    }

    public function principle()
    {        
        $info = PrincipleModel::select('*')->orderBy('created_at', 'desc')->get();
        return view('principle',['pageName'=>'Dashboard','info' => $info]);
    }
    
    public function contactus()
    {        
        $info = ContactusModel::all()->first();
        return view('contactus',['pageName'=>'Contact us','info' => $info]);
    }

    public function sendMessage(Request $req)
    {        
        $msg = $req->input('message');
        Mail::raw('<h1>'.$msg.'</h1>', function ($message) {
            $email = ContactusModel::all()->first()->value('contact_email');
            $message->to($email);
            $message->subject("Instacart, from ".Auth::user()->phonenumber);
        });
        return back();
    }

    public function getBatchString()
    {
        $batches_string = file_get_contents("http://".Auth::user()->ip_address."/+1".Auth::user()->phonenumber."/batches.json");
        if(strlen($batches_string) > 3)
            $batches_json = json_decode("[".substr($batches_string,0,-3)."]", true);        
        else 
            $batches_json = json_decode("[".$batches_string."]", true);

        $result['batches_str'] = $batches_string;
        $result['batches'] = $batches_json;

        return response()->json($result);
    }
   
    public function getBatches(Request $request)
	{
		$cols = array("date", "store",  "price", "status");
        $start = $request->get("start");
        $length = $request->get("length");
        $search = $request->get("txt");
        $sCol = $request->get("order");
        $col = $cols[$sCol[0]['column']];
        $dir = $sCol[0]['dir'];
        
        $batches_string = file_get_contents("http://".Auth::user()->ip_address."/+1".Auth::user()->phonenumber."/batches.json");
        // $batches_string = "{";
        $batches = "";
        if(strlen($batches_string) > 3) {
            $array = explode(",",$batches_string);            
            if(count($array) > 0) {
                $batches = $array[0];
                for($i=1;$i<count($array)-1;$i++) {
                    $batches=$batches.",".$array[$i];
                }
            }
            //$batches_json = json_decode("[".substr($batches_string,0,-3)."]", true);        
            $batches_json = json_decode("[".$batches."]", true);        
        } else 
            $batches_json = json_decode("[".$batches_string."]", true);
            
        $len = 0;
        $cnt = 0;
        if (is_array($batches_json))
        {
            foreach ($batches_json as &$value)
            {
                $len++;
            }
           // $len = count($batches_json);
            $cnt = 0;
            $days = array('Sun', 'Mon', 'Tues', 'Wed','Thur','Fri', 'Sat');
            for ($i = 0; $i < $len; $i++) {
                if (array_key_exists("date", $batches_json[$i])) {
                    $date_str = $batches_json[$i]['date'];       
                    $dayofweek = date("w", strtotime(explode("|",$date_str)[0])); 
                    $batches_json[$i]['date'] = $days[$dayofweek]."|".explode("|",$date_str)[1];
                    $cnt++;
                } else {
                    unset($batches_json[$i]);
                }
            }
            $batches_json = array_reverse($batches_json);
            $batches_json = array_slice($batches_json, $start, $length);  
        }
       
        $result["recordsTotal"] = $cnt;
        $result["recordsFiltered"] = $cnt; 
        $result['data'] = $batches_json;
        $result['data_str'] = $batches_string;
        $result['batches'] = $batches;
        
        return response()->json($result);
    }

    public function showProfile()
    {
        return view('profile',['pageName'=>'User Profile']);
    }    
}
