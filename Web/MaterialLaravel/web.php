<?php
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
//     // Ignores notices and reports all other kinds... and warnings
//     error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
//     // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
// }

Route::get('/', 'UserController@index');
Route::get('/login', ['as' => 'login', 'uses' => 'UserController@index']);
Route::post('/login', 'UserController@login');
Route::post('/signup', 'UserController@postSignup');
Route::get('/register', 'UserController@showSignup');

Route::get('/logout', 'UserController@logout');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    // return what you want
});

Route::get('/runOff', 'HomeController@runOff');

Route::group(['middleware' => ['check']], function () {
    Route::get('/home', 'HomeController@index')->middleware('auth');

    Route::post('/home/postContinue', 'HomeController@postContinue')->middleware('auth');
    Route::get('/home/showContinue', 'HomeController@showContinue')->middleware('auth');
    Route::get('/saveProfile', 'HomeController@saveProfile')->middleware('auth');
    Route::get('/home/saveProfile', 'HomeController@saveProfile')->middleware('auth');
    Route::get('/profile', 'HomeController@showProfile')->middleware('auth');

    // Route::get('/home/turnOn', 'HomeController@turnOn')->middleware('auth');
	// Route::get('/home/turnOff', 'HomeController@turnOff')->middleware('auth');
	Route::get('/turnOn', 'HomeController@turnOn')->middleware('auth');
    Route::get('/turnOff', 'HomeController@turnOff')->middleware('auth');

	Route::get('/power', 'HomeController@showContinue')->middleware('auth');
    Route::get('/getBatches', 'HomeController@getBatches')->middleware('auth');
    Route::get('/getBatchString', 'HomeController@getBatchString')->middleware('auth');

    Route::get('/subscription', 'HomeController@subscription')->middleware('auth');
    Route::get('/payment', 'HomeController@payment')->middleware('auth');
    Route::post('/charge', 'HomeController@charge')->middleware('auth');

    Route::get('/principle', 'HomeController@principle')->middleware('auth');
    // Route::get('/contactus', 'HomeController@contactus')->middleware('auth');
    Route::post('/sendMessage', 'HomeController@sendMessage')->middleware('auth');
    
    Route::get('/contactus', 'ChattingController@index')->middleware('auth');
    Route::get('/getMessagesByUserid', 'ChattingController@getMessagesByUserid')->middleware('auth')->middleware('auth');
    Route::get('/getNewMessageByUserid', 'ChattingController@getNewMessageByUserid')->middleware('auth')->middleware('auth');
    Route::get('/setNewMessageAsOld', 'ChattingController@setNewMessageAsOld')->middleware('auth')->middleware('auth');
    
    Route::post('/clientSendMessage', 'ChattingController@clientSendMessage')->middleware('auth');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

// Route::group(['middleware' => 'auth'], function () {
// 	Route::get('table-list', function () {
// 		return view('pages.table_list');
// 	})->name('table');

// 	Route::get('typography', function () {
// 		return view('pages.typography');
// 	})->name('typography');

// 	Route::get('icons', function () {
// 		return view('pages.icons');
// 	})->name('icons');

// 	Route::get('map', function () {
// 		return view('pages.map');
// 	})->name('map');

// 	Route::get('notifications', function () {
// 		return view('pages.notifications');
// 	})->name('notifications');

// 	Route::get('rtl-support', function () {
// 		return view('pages.language');
// 	})->name('language');

// 	Route::get('upgrade', function () {
// 		return view('pages.upgrade');
// 	})->name('upgrade');
// });

// Route::group(['middleware' => 'auth'], function () {
// 	Route::resource('user', 'UserController', ['except' => ['show']]);
// 	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
// 	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
// 	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
// });

