<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{
    protected $table = 'users';
    protected $fillable = ['id','name' ,'phonenumber','email', 'city', 'password', 'ip_address', 'extension', 'is_running', 'active', 'subscription_expired','subscription_started_date','	subscription_end_date', 'paid_amount'];
}
