@extends('layouts.main')
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>{{ ucfirst($pageName) }}</h2>
    </header>
    <div class="text-right">
        <button class="btn btn-primary add-user"><i class="fa fa-plus"></i> Add User</button>
    </div>
    <!--start content  -->
    <div class="panel-body">
       <table id="users-table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>City</th>
                    <th>IP Address</th>
                    <th>Ext</th>                    
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Run</th>
                    <th width="8%">Pwd</th>
                    <th width="8%">Edit</th>
                    <th width="8%">Delete</th>
                    <th width="8%">Active</th>
                </tr>
            </thead>
        </table>
    </div>
<!--end content  -->
</section>
<div class="modal fade" id="add-user-modal" role="dialog">
    <div class="modal-dialog modal-lg">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title add-submenu-title text-center">
                {{--Add User--}}
            </h2>
        </div>
        <div class="modal-body add-submenu-content">
           <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="user-name">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control signup-field" name="user-name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="user-phone">Phone:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control signup-field" name="user-phone">
                    </div>
                </div>
                <div class="form-group">
                   <label class="control-label col-sm-2" for="user-email">Email:</label>
                   <div class="col-sm-10">
                       <input type="text" class="form-control signup-field" name="user-email">
                   </div>
                </div>
                <div class="form-group">
                   <label class="control-label col-sm-2" for="user-ip-address">IP Address:</label>
                   <div class="col-sm-10">
                       <input type="text" class="form-control signup-field" name="user-ip-address">
                   </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="user-password">Password:</label>
                  <div class="col-sm-10">
                    <input class="form-control signup-field" name="user-password" type="password">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="user-cpassword">Confirm password:</label>
                  <div class="col-sm-10">
                    <input class="form-control signup-field" name="user-cpassword" type="password">
                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success add-user-save" data-dismiss="modal"> &nbsp;Add&nbsp; </button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<div id="dialog" class="modal-block modal-block-lg mfp-hide zoom-anim-dialog modal-header-color modal-block-primary" style="display:block">
    <section class="panel">
        <header class="panel-heading">
            <h1 class="panel-title text-center text-white">
                <img src="{{ url('assets/images/avatar.png') }}" alt="Joseph Doe" class="img-circle"
                data-lock-picture="assets/images/avatar.png" height="70" />
            </h1>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="modal-text">
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button id="dialogConfirm" class="btn btn-primary modal-confirm">&nbsp;Save&nbsp;</button>
                    <button id="dialogCancel" class="btn btn-default modal-dismiss">Cancel</button>
                </div>
            </div>
        </footer>
    </section>
</div>

<div class="modal fade" id="pwd-change-modal" role="dialog">
    <div class="modal-dialog modal-lg">  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title add-submenu-title text-center">
                {{--Add User--}}
            </h2>
        </div>
        <div class="modal-body add-submenu-content">
           <div class="form-horizontal">                
                <input class="form-control pwd-change-field" name="pwd-change-user-id" type="hidden">
                <div class="form-group">
                  <label class="control-label col-sm-2" for="user-password">Password:</label>
                  <div class="col-sm-10">
                    <input class="form-control pwd-change-field" name="pwd-change-user-password" type="password">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="user-cpassword">Confirm password:</label>
                  <div class="col-sm-10">
                    <input class="form-control pwd-change-field" name="pwd-change-user-cpassword" type="password">
                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success pwd-change-save" data-dismiss="modal"> &nbsp;Change&nbsp; </button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('assets/js/users.js') }}"></script>
@endsection
