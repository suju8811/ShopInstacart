var uTable;
var search_txt='';
var fcode = [];
var _token = $("#csrf_token").val();
$(function(){  
    uTable = $('#users-table').dataTable({
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "language":{
        	"sSearch":"",
        	"sSearchPlaceholder":"Search",
        	"paginate":{
        		"previous":"<",
        		"next":">"
        	}
        },
        "lengthMenu": [[7, 15, 50, 100], [7, 15, 50, 100]],
        "ajax": {
            "url": "getUsers",
            "type": "GET",
            "data":function(key){
                key.txt=search_txt;
                // key._token=$('#token_id').val();
            }
        }, 
        // "dom": '<"toolbar">rtip',
        "columnDefs": [ 
            {
                "targets": "_all",
                "searchable": false
            },
            { 
            	"orderable": false,
                "targets": [ 4,5 ]
            },
         ],
         "columns":[
         	{
         		'data':'id',
         		"render":function (data,type,row,meta) {
	                var page=uTable.api().page();
	                return page*meta.settings._iDisplayLength+meta.row+1+"<span style='display:none' class='client-id'>"+data+"</span>";
	            }
         	},
			{
				'data':'name'
			},
         	{
         		'data':'phonenumber'
			},
			{
				'data':'email'
			},
			{
				'data':'city'
			},
		    {
			    'data':'ip_address'
			},
			{
			    'data':'extension'
			},
			{
				'data':'subscription_started_date'
			},
			{
				'data':'subscription_end_date'
			},
			{
			    'data':'is_running'
			},
			{
				'data':'editPassword',
				"render":function (data,type,row,meta) {
					var el="<a href='#dialog' class='btn btn-primary btn-xs editPasswordBtn'><i class='fa fa-key'></i> Edit</a>";
					return el;
				}
			},
			{
				'data':'edit',
				"render":function (data,type,row,meta) {
				   var el="<a href='#dialog' class='btn btn-primary btn-xs editBtn'><i class='fa fa-edit'></i> Edit</a>"+
				   "<a href='#dialog' class='btn btn-success btn-xs cancelBtn' style='margin-right: 5px;'><i class='fa fa-remove'></i></a>"+
				   "<a href='#dialog' class='btn btn-success btn-xs saveBtn'><i class='fa fa-save'></i></a>";
				   return el;
			   }
			},  
         	{
         		'data':'updated_at',
         		"render":function (data,type,row,meta) {
	                var el="<a href='#dialog' class='btn btn-danger btn-xs modal-basic deleteBtn'><i class='fa fa-trash-o'></i> Delete</a>";
	                return el;
	            }
         	},  
         	{
         		'data':'active',
         		"render":function (data,type,row,meta) {
	                if(data==0){
	                	var el = "<button class='btn btn-primary btn-xs activeBtn'><i class='fa fa-thumbs-o-up'></i> Accept</button>";
	                } else {
	                	var el = "<button class='btn btn-warning btn-xs activeBtn'><i class='fa fa-thumbs-o-down'></i> Refuse</button>";
	                }
	                return el;
	            }
         	},
         ],
         "footerCallback": function (){
         	// $('#users-table_filter').html("<input type='search' class='form-control users-search' placeholder='Search by userid' aria-controls='users-table' onkeyup='whichButton(event)'>");
         	$("#users-table_filter input").off('keyup');
         	$("#users-table_filter input").off('cut');
         	$("#users-table_filter input").off('keypress');
         	$("#users-table_filter input").off('cut');
         	$("#users-table_filter input").off('search');
         	$("#users-table_filter input").off('input');
         	$("#users-table_filter input").off('paste');
         	setTimeout(defineFunction,500);
        }         
    });
    
    $(".add-user").click(function(){
		$("#add-user-modal").modal();
	});
	$(".add-user-save").click(function(){
		var st = 0;
		$(".signup-field").each(function(){
			if(!$(this).val().length){
				st = 1;;
			}
		});
		if(st==1){
			message(msg.EmptyField,"warning");
			return false;
		}
		name = $("[name='user-name']").val();
		ip_address = $("[name='user-ip-address']").val();
		phone = $("[name='user-phone']").val();
		email = $("[name='user-email']").val();
		password = $("[name='user-password']").val();
		cpassword = $("[name='user-cpassword']").val();
		if(password!=cpassword){
			message(msg.MisMatchPassword,"warning");
			return false;
		}
		$.ajax({
            url: "user_signup",
            type:'post',
            data:{
            	_token:_token,
            	name:name,
            	ip_address:ip_address,
				phone:phone,
				email:email,
            	password:password
            },
            success: function(result){
            	if(result=="success"){
            		message(msg.SuccessSignUp,"success");
            		$(".signup-field").each(function(){
						$(this).val("");
					});
					uTable.api().ajax.reload();
            	} else {
            		$(".signup-field").each(function(){
						$(this).val("");
					});
            		message(msg.RepeatedPhonenumber,"warning");
            		return false;
            	}
            }
        });
	});
});

function defineFunction(){
	
	$(".editPasswordBtn").click(function(){
		var id = $(this).parents("tr").find("td").find(".client-id").text();		
		$("#pwd-change-modal input[name='pwd-change-user-id']").val(id);
		$("#pwd-change-modal").modal();
	});

	$(".pwd-change-save").click(function(){
		var st = 0;
		$(".pwd-change-field").each(function(){
			if(!$(this).val().length){
				st = 1;;
			}
		});
		if(st==1){
			message(msg.EmptyField,"warning");
			return false;
		}
		user_id = $("[name='pwd-change-user-id']").val();
		password = $("[name='pwd-change-user-password']").val();
		cpassword = $("[name='pwd-change-user-cpassword']").val();
		if(password!=cpassword){
			message(msg.MisMatchPassword,"warning");
			return false;
		}
		$.ajax({
            url: "user_pwd_change",
            type:'post',
            data:{
            	_token:_token,
            	id:user_id,
            	password:password
            },
            success: function(result){
            	if(result=="success"){
					$(".pwd-change-field").each(function(){
						$(this).val("");
					});
            		message(msg.SuccessSavePassword,"success");
            	} else {
            		$(".pwd-change-field").each(function(){
						$(this).val("");
					});
            		message(msg.FailedSaveProfile,"warning");
            		return false;
            	}
            }
        });
	});

	$(".editBtn").click(function(){
		
		$(this).css("display","none");
		$(this).siblings('.saveBtn').css("display","inline-block");
		$(this).siblings('.cancelBtn').css("display","inline-block");
		i=0;
		$(this).parents("tr").find("td").each(function(){
			if(i>0 && i<10) {

				fcode[i] = $(this).text();
				$(this).html("<input class='form-control' value='"+fcode[i]+"' style='text-align:center;'>");
			}
			i++;
		});
	});
	$(".cancelBtn").click(function(){
		$(this).siblings('.editBtn').css("display","inline-block");
		$(this).siblings('.saveBtn').css("display","none");
		$(this).css("display","none");
		i=0;
		$(this).parents("tr").find("td").each(function(){
			if(i>0 && i<10) {
				$(this).html(fcode[i]);
			}
			i++;
		});
	});
	$(".saveBtn").click(function(){
		var id = $(this).parents("tr").find("td").find(".client-id").text();
		$(this).siblings('.editBtn').css("display","inline-block");
		$(this).siblings('.cancelBtn').css("display","none");
		$(this).css("display","none");
		i=0;
		$(this).parents("tr").find("td").each(function(){
			if(i>0 && i<10) {
				code = $(this).find("input").val();
				$(this).html(code);
				if(i==1) { name = code; }
				if(i==2) { phone = code; }
				if(i==3) { email = code; }
				if(i==4) { city = code; }
				if(i==5) { ip_address = code; }
				if(i==6) { extension = code; }
				if(i==7) { start_date = code; }
				if(i==8) { end_date = code; }
				if(i==9) { is_running = code; }
			}
			i++;
		});

		$.ajax({
            url: "editUser",
            type:'get',
            data:{
            	id:id,
            	name:name,
				phonenumber:phone,
				email:email,
				city:city,
				ip_address:ip_address,
				extension:extension,
				start_date:start_date,
				end_date:end_date,
				is_running:is_running,
            },
            success: function(result){}
        });
	});	
	$(".activeBtn").click(function(){
		var id = $(this).parents("tr").find("td").find(".client-id").text();
		if($(this).hasClass("btn-primary")){
			$(this).removeClass("btn-primary");
			$(this).addClass("btn-warning");
			$(this).text("Refuse");
			$(this).prepend("<i class='fa fa-thumbs-o-down'></i>&nbsp;");
			
			$.ajax({
	            url: "acceptUser",
	            type:'get',
	            data:{
	            	id:id,
	            },
	            success: function(result){}
	        });

		} else {
			$(this).addClass("btn-primary");
			$(this).removeClass("btn-warning");
			$(this).text("Accept");
			$(this).prepend("<i class='fa fa-thumbs-o-up'></i>&nbsp;");

			$.ajax({
	            url: "refuseUser",
	            type:'get',
	            data:{
	            	id:id,
	            },
	            success: function(result){}
	        });

		}
	});
	$(".deleteBtn").click(function(){
		if(confirm("Do you want to delete current user?")){
			var id = $(this).parents("tr").find("td").find(".client-id").text();
			$.ajax({
	            url: "deleteUser",
	            type:'get',
	            data:{
	            	id:id,
	            },
	            success: function(result){
	            	uTable.api().ajax.reload();
	            }
	        });
		}
	});
	$("#users-table_filter").keyup(function(event){
		if(event.keyCode===13){
	        search_txt=$("#users-table_filter input").val();
    		uTable.fnFilter();
		}
	});
}
// function whichButton(event) {
//     if (event.keyCode == 13) {
//         search_txt=$(".users-search").val();
//     	uTable.fnFilter();
//     }
// }

